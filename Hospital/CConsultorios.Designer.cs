﻿namespace Hospital
{
    partial class CConsultorios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnResgistar = new System.Windows.Forms.Button();
            this.txtIdCon = new System.Windows.Forms.TextBox();
            this.lblIdCn = new System.Windows.Forms.Label();
            this.lblNombreCon = new System.Windows.Forms.Label();
            this.lblNombreConsultorio = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.FlatAppearance.BorderSize = 0;
            this.btnSeleccionar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnSeleccionar.Click += new System.EventHandler(this.btnSeleccionar_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.FlatAppearance.BorderSize = 0;
            this.btnAtras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatAppearance.BorderSize = 0;
            this.btnBuscar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(158, 487);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(124, 31);
            this.btnEliminar.TabIndex = 29;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnResgistar
            // 
            this.btnResgistar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnResgistar.FlatAppearance.BorderSize = 0;
            this.btnResgistar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnResgistar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResgistar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResgistar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnResgistar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnResgistar.Location = new System.Drawing.Point(28, 487);
            this.btnResgistar.Name = "btnResgistar";
            this.btnResgistar.Size = new System.Drawing.Size(124, 31);
            this.btnResgistar.TabIndex = 30;
            this.btnResgistar.Text = "Registrar";
            this.btnResgistar.UseVisualStyleBackColor = false;
            this.btnResgistar.Click += new System.EventHandler(this.btnResgistar_Click);
            // 
            // txtIdCon
            // 
            this.txtIdCon.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdCon.Location = new System.Drawing.Point(175, 450);
            this.txtIdCon.Name = "txtIdCon";
            this.txtIdCon.Size = new System.Drawing.Size(106, 26);
            this.txtIdCon.TabIndex = 31;
            this.txtIdCon.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblIdCn
            // 
            this.lblIdCn.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdCn.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblIdCn.Location = new System.Drawing.Point(24, 450);
            this.lblIdCn.Name = "lblIdCn";
            this.lblIdCn.Size = new System.Drawing.Size(136, 28);
            this.lblIdCn.TabIndex = 32;
            this.lblIdCn.Text = "IdConsultorio:";
            // 
            // lblNombreCon
            // 
            this.lblNombreCon.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreCon.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblNombreCon.Location = new System.Drawing.Point(287, 450);
            this.lblNombreCon.Name = "lblNombreCon";
            this.lblNombreCon.Size = new System.Drawing.Size(207, 28);
            this.lblNombreCon.TabIndex = 33;
            this.lblNombreCon.Text = "Nombre Consultorio:";
            // 
            // lblNombreConsultorio
            // 
            this.lblNombreConsultorio.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreConsultorio.Location = new System.Drawing.Point(500, 449);
            this.lblNombreConsultorio.Name = "lblNombreConsultorio";
            this.lblNombreConsultorio.Size = new System.Drawing.Size(252, 26);
            this.lblNombreConsultorio.TabIndex = 34;
            this.lblNombreConsultorio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CConsultorios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 549);
            this.Controls.Add(this.lblNombreConsultorio);
            this.Controls.Add(this.lblNombreCon);
            this.Controls.Add(this.lblIdCn);
            this.Controls.Add(this.txtIdCon);
            this.Controls.Add(this.btnResgistar);
            this.Controls.Add(this.btnEliminar);
            this.Name = "CConsultorios";
            this.Text = "CConsultorios";
            this.Load += new System.EventHandler(this.CConsultorios_Load);
            this.Controls.SetChildIndex(this.btnSeleccionar, 0);
            this.Controls.SetChildIndex(this.btnAtras, 0);
            this.Controls.SetChildIndex(this.txtNombreM, 0);
            this.Controls.SetChildIndex(this.lblNombreM, 0);
            this.Controls.SetChildIndex(this.btnBuscar, 0);
            this.Controls.SetChildIndex(this.btnEliminar, 0);
            this.Controls.SetChildIndex(this.btnResgistar, 0);
            this.Controls.SetChildIndex(this.txtIdCon, 0);
            this.Controls.SetChildIndex(this.lblIdCn, 0);
            this.Controls.SetChildIndex(this.lblNombreCon, 0);
            this.Controls.SetChildIndex(this.lblNombreConsultorio, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnEliminar;
        public System.Windows.Forms.Button btnResgistar;
        private System.Windows.Forms.TextBox txtIdCon;
        private System.Windows.Forms.Label lblIdCn;
        private System.Windows.Forms.Label lblNombreCon;
        private System.Windows.Forms.TextBox lblNombreConsultorio;
    }
}