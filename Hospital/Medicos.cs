﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;


namespace Hospital
{
    public partial class Medicos : Form

    {
        public Medicos()
        {
            InitializeComponent();
        }

        private void Limpiar()
        {
            txtid.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtAccount.Clear();
            txtPass.Clear();
        }
        
        private void Medicos_Load(object sender, EventArgs e)
        {
            //string cmd = string.Format("SELECT Nombre,Apellido FROM dbo.Medico WHERE IdMedico="+Login.id);
            //string cmd2 = string.Format("SELECT * FROM dbo.Especialidad WHERE IdMedico="+Login.id);

            //DataSet ds = Utilidades.Ejecutar(cmd);
           // DataSet ds2 = Utilidades.Ejecutar(cmd2);

            //txtid.Text = Login.id;
           // txtNombre.Text = ds.Tables[0].Rows[0]["Nombre"].ToString();
            //txtApellido.Text = ds.Tables[0].Rows[0]["Apellido"].ToString();
            //txtEspecialidad.Text = ds2.Tables[0].Rows[0]["Especialidad"].ToString();

            //string url = ds.Tables[0].Rows[0]["foto"].ToString();
            //pictureBox1.Image = Image.FromFile(url);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            

            try
            {
                string cmd = string.Format("INSERT INTO dbo.Medico (Nombre, Apellido, Account, Mpassword)" +
                "VALUES ('{0}','{1}','{2}','{3}')", txtNombre.Text.Trim(), txtApellido.Text.Trim(), txtAccount.Text.Trim(), txtPass.Text.Trim());
                DataSet ds = Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se registrado un nuevo medico");
                Limpiar();
            }
            catch (Exception error)
            {
                MessageBox.Show("Error al registrar medico Verifique esten llenos los campos. " + error.Message);
            }
        }



        private void btnMedicoC_Click(object sender, EventArgs e)
        {
            ConsultarMedico cm = new ConsultarMedico();
            cm.ShowDialog();

            if (cm.DialogResult == DialogResult.OK)
            {
                txtid.Text = cm.dataGridView1.Rows[cm.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtNombre.Text = cm.dataGridView1.Rows[cm.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
                txtApellido.Text = cm.dataGridView1.Rows[cm.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
                
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                string cmd = "DELETE FROM dbo.Medico WHERE IdMedico = " + txtid.Text.Trim();
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado un medico. ", "Eliminar");
                Limpiar();
                


            }
            catch (Exception error)
            {
                MessageBox.Show("Error al tratar de eliminar medico. " + error.Message);
            }
        }

        private void btnEspecialidad_Click(object sender, EventArgs e)
        {
            txtid.Focus();
            txtEspecialidad.Focus();
            try
            {

                string cmd = string.Format("INSERT INTO dbo.Especialidad (IdMedico, Especialidad)" +
                    "VALUES ('{0}','{1}')", txtid.Text.Trim(),txtEspecialidad.Text.Trim());

                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha ligado la especialidad con un medico. ", "Especialidad");
                Limpiar();



            }
            catch (Exception error)
            {
                MessageBox.Show("Error al tratar de eliminar medico. " + error.Message);
            }
        }

        private void btnVerEspe_Click(object sender, EventArgs e)
        {
            VerEspecialidad ve = new VerEspecialidad();
            ve.ShowDialog();

            if(DialogResult== DialogResult.OK)
            {

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
           

        }
    }
}
