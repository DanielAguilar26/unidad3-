﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Hospital
{
    public partial class VerCitas : Consultas
    {
        public VerCitas()
        {
            InitializeComponent();
        }

        private void VerCitas_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LlenarDataGVM("Cita").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNombreM.Text.Trim()) == false)
            {
                try
                {
                    
                   //DataSet ds;
                   //string cmd = "SELECT * FROM Cita Where LIKE ")+txtNombreM.Text.Trim()+");
                   //ds = Utilidades.Ejecutar(cmd);

                   //dataGridView1.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error. " + error.Message);
                }

                try
                {
                    string cmd = string.Format("EXEC mostrarCitas '{0}' @IdPaciente =", Citas.idP);
                    DataSet ds = Utilidades.Ejecutar(cmd);
                    
                    

                    dataGridView1.DataSource = ds.Tables[0];


                    //string cmd = string.Format("SELECT Nombre,Apellido FROM dbo.Medico WHERE IdMedico="+Login.id);
                    //string cmd2 = string.Format("SELECT * FROM dbo.Especialidad WHERE IdMedico="+Login.id);

                    //DataSet ds = Utilidades.Ejecutar(cmd);
                    // DataSet ds2 = Utilidades.Ejecutar(cmd2);

                    //txtid.Text = Login.id;
                    // txtNombre.Text = ds.Tables[0].Rows[0]["Nombre"].ToString();
                    //txtApellido.Text = ds.Tables[0].Rows[0]["Apellido"].ToString();
                    //txtEspecialidad.Text = ds2.Tables[0].Rows[0]["Especialidad"].ToString();

                    //string url = ds.Tables[0].Rows[0]["foto"].ToString();
                    //pictureBox1.Image = Image.FromFile(url);

                    //DataSet ds;
                    //string cmd = "SELECT * FROM Cita Where LIKE ")+txtNombreM.Text.Trim()+");
                    //ds = Utilidades.Ejecutar(cmd);

                    //dataGridView1.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Error al mostrar cita. ", "Error" + error.Message);
                }

            }
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
