﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Hospital
{
    public partial class ConsultarMedico : Consultas
    {
        public ConsultarMedico()
        {
            InitializeComponent();
        }

        private void ConsultarMedico_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LlenarDataGVM("Medico").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtNombreM.Text.Trim())==false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "SELECT * FROM Medico Where Nombre LIKE ('%" + txtNombreM.Text.Trim() + "%')";
                    ds = Utilidades.Ejecutar(cmd);

                    dataGridView1.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error. "+error.Message);
                }
            }
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
