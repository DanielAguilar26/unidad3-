﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Hospital
{
    public partial class VerEspecialidad : Consultas
    {
        public VerEspecialidad()
        {
            InitializeComponent();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNombreM.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "SELECT * FROM Especialidad Where Especialidad LIKE ('%" + txtNombreM.Text.Trim() + "%')";
                    ds = Utilidades.Ejecutar(cmd);

                    dataGridView1.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error. " + error.Message);
                }
            }
        }

        private void VerEspecialidad_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LlenarDataGVM("Especialidad").Tables[0];
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            
        }
    }
}
