use master
go

if DB_ID ('Hospital')is not null
	Begin 
		Drop database Hospital 
	End
go


create database Hospital 
go

use Hospital 
go

If OBJECT_ID ('dbo.Medico')is not null
	Begin 
		Drop table dbo.Medico 
	End
go

Create table dbo.Medico (
IdMedico    int identity (1,1) not null,
Nombre      varchar(20) not null,
Apellido    varchar(20)not null,
Account     varchar(50)not null,
Mpassword   varchar(30)not null,
foto        varchar(400)not null,
constraint Pk_IdMedico Primary key (IdMedico) 
);
go

If OBJECT_ID ('dbo.Especialidad')is not null
	Begin 
		Drop table dbo.Especialidad
	End
go

Create table dbo.Especialidad(
IdMedico      int not null,
Especialidad  varchar(30)not null,
constraint Fk_IdMedico Foreign Key (IdMedico) 
references dbo.Medico (IdMedico),

constraint PKk_Especialidad Primary Key (IdMedico,Especialidad) 
);
go

If OBJECT_ID ('dbo.Paciente')is not null
	Begin 
		Drop table dbo.Paciente
	End
go

Create table dbo.Paciente (
IdPaciente   int identity (1,1) not null,
Nombre       varchar(20)not null,
Apellido     varchar(22)not null,
Sexo         varchar(10)not null
constraint Pk_IdPaciente primary key (IdPaciente)
);
Go

If OBJECT_ID ('dbo.Consultorio')is not null
	Begin
		Drop table dbo.Consultorio
	End
go

Create table dbo.Consultorio (
IdConsultorio int identity (1,1)not null,
NombreConsultorio varchar(15)not null,

Constraint Pk_IdConsultorio Primary Key (IdConsultorio)
);
Go

If OBJECT_ID ('dbo.Cita')is not null
	Begin 
		Drop table dbo.Cita
	End
go

Create table dbo.Cita(
Folio         int identity (1,1)not null,
IdMedico      int not null,
IdPaciente    int not null, 
IdConsultorio int not null,
Fecha       date not null,
FechaProx   date,
Observaciones varchar(100)not null,
constraint Pk_Cita primary key (Folio),

constraint Fk_medicoc foreign key (IdMedico)
references dbo.Medico(IdMedico),

constraint Fk_pasiente foreign key (IdPaciente)
references dbo.Paciente(IdPaciente),

constraint Fk_consultorio foreign key (IdConsultorio)
references dbo.Consultorio(IdConsultorio),
);
go





