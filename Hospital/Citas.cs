﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Hospital
{
    public partial class Citas : Form
    {
        public static String idP = "";
        public Citas()
        {
            InitializeComponent();
           
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        

        private void btnMedico_Click(object sender, EventArgs e)
        {
            ConsultarMedico cm = new ConsultarMedico();
            cm.ShowDialog();

            if(cm.DialogResult == DialogResult.OK)
            {
                txtIdMedico.Text = cm.dataGridView1.Rows[cm.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtMedico.Text = cm.dataGridView1.Rows[cm.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
            }
        }

        private void btnPaciente_Click(object sender, EventArgs e)
        {
            ConsultarPacientes cp = new ConsultarPacientes();
            cp.ShowDialog();

            if(cp.DialogResult == DialogResult.OK)
            {
                idP=txtIdPaciente.Text = cp.dataGridView1.Rows[cp.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtPaciente.Text = cp.dataGridView1.Rows[cp.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
            }
        }

        private void btnConsultorio_Click(object sender, EventArgs e)
        {
            CConsultorios cc = new CConsultorios();
            cc.ShowDialog();

            if(cc.DialogResult == DialogResult.OK)
            {
                txtIdConsultorio.Text = cc.dataGridView1.Rows[cc.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtNConsultorio.Text = cc.dataGridView1.Rows[cc.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
                txtFecha.Focus();
            }
        }

        private void btnAgendar_Click(object sender, EventArgs e)
        {
           // string fecha = "GETDATE()";
            //string fechaAprox = "DATEADD(DAY, 5, GETDATE())";

            try
            {
                string cmd = string.Format("INSERT INTO dbo.Cita(IdMedico, IdPaciente, IdConsultorio, Fecha, FechaProx, Observaciones)"+
                "VALUES('{0}','{1}','{2}','{3}','{4}','{5}')",
                txtIdMedico.Text.Trim(),txtIdPaciente.Text.Trim(),txtIdConsultorio.Text.Trim(),
                txtFecha.Text,txtFechaProx.Text,txtObserva.Text.Trim());
                
                DataSet ds = Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha agendado una cita.");
            }
            catch (Exception error)
            {
                MessageBox.Show("Error al agendar la cita Verifique esten llenos los campos. " + error.Message);
            }
        }

        private void btnCitas_Click(object sender, EventArgs e)
        {
            VerCitas vc = new VerCitas();
            vc.ShowDialog();
        }
    }
}
