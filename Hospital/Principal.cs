﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Hospital
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
         
        }

        private void abrirFormPanel(Object Form)
        {
            if(this.Contenedor.Controls.Count>0)
            {
                this.Contenedor.Controls.RemoveAt(0);
               
            }            
            Form fm = Form as Form;
            fm.TopLevel = false;
            fm.Dock = DockStyle.Fill;
            this.Contenedor.Controls.Add(fm);
            this.Contenedor.Tag = fm;
            fm.Show();
        }

        private void pcbMenu_Click(object sender, EventArgs e)
        {
            if(MenuVertical.Width == 200)
            {
                MenuVertical.Width = 115;
                ocultarTextoBoton();
            }
            else
            {
                MenuVertical.Width = 200;
                ocultarTextoBoton();
            }
        }

        private void btnCita_Click(object sender, EventArgs e)
        {
            
            abrirFormPanel(new Citas());
        }

        private void ocultarTextoBoton()
        {
            if(MenuVertical.Width == 115)
            {
                btnCita.Text = "";
                btnMedico.Text = "";
                btnPacientes.Text = "";
                btnConsultorio.Text = "";
                btnAdmin.Text = "";

            }
            else
            {
                btnCita.Text = "Citas";
                btnMedico.Text = "Medicos";
                btnPacientes.Text = "Pacientes";
                btnConsultorio.Text = "Consultorios";
                btnAdmin.Text = "Administrar";

            }
        }

        private void btnMedico_Click(object sender, EventArgs e)
        {
            abrirFormPanel(new Medicos());
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnPacientes_Click(object sender, EventArgs e)
        {
            abrirFormPanel(new Pacientes());
        }

        private void Contenedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿En realidad desea salir?","Confirmación",
                MessageBoxButtons.YesNo,MessageBoxIcon.Information,MessageBoxDefaultButton.Button1)==DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnConsultorio_Click(object sender, EventArgs e)
        {
            abrirFormPanel(new CConsultorios());
        }
    }
}
