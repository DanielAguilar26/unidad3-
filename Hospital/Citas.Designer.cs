﻿namespace Hospital
{
    partial class Citas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCita = new System.Windows.Forms.Label();
            this.lblIdMedico = new System.Windows.Forms.Label();
            this.lblFolio = new System.Windows.Forms.Label();
            this.lblIdPaciente = new System.Windows.Forms.Label();
            this.lblidConsultorio = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.lblFechaPro = new System.Windows.Forms.Label();
            this.lblOvservaciones = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNConsultorio = new System.Windows.Forms.TextBox();
            this.txtPaciente = new System.Windows.Forms.TextBox();
            this.lblPaciente = new System.Windows.Forms.Label();
            this.lblConsultorio = new System.Windows.Forms.Label();
            this.txtIdConsultorio = new System.Windows.Forms.TextBox();
            this.txtIdPaciente = new System.Windows.Forms.TextBox();
            this.txtMedico = new System.Windows.Forms.TextBox();
            this.lblMedico = new System.Windows.Forms.Label();
            this.txtIdMedico = new System.Windows.Forms.TextBox();
            this.txtFolio = new System.Windows.Forms.TextBox();
            this.grbCita = new System.Windows.Forms.GroupBox();
            this.lblFormato = new System.Windows.Forms.Label();
            this.txtObserva = new System.Windows.Forms.TextBox();
            this.txtFechaProx = new System.Windows.Forms.TextBox();
            this.txtFecha = new System.Windows.Forms.TextBox();
            this.btnMedico = new System.Windows.Forms.Button();
            this.btnPaciente = new System.Windows.Forms.Button();
            this.btnConsultorio = new System.Windows.Forms.Button();
            this.btnCitas = new System.Windows.Forms.Button();
            this.btnAgendar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.grbCita.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCita
            // 
            this.lblCita.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCita.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblCita.Location = new System.Drawing.Point(666, 9);
            this.lblCita.Name = "lblCita";
            this.lblCita.Size = new System.Drawing.Size(176, 28);
            this.lblCita.TabIndex = 8;
            this.lblCita.Text = "CITA MEDICA";
            // 
            // lblIdMedico
            // 
            this.lblIdMedico.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdMedico.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblIdMedico.Location = new System.Drawing.Point(6, 27);
            this.lblIdMedico.Name = "lblIdMedico";
            this.lblIdMedico.Size = new System.Drawing.Size(108, 28);
            this.lblIdMedico.TabIndex = 25;
            this.lblIdMedico.Text = "IdMedico:";
            // 
            // lblFolio
            // 
            this.lblFolio.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFolio.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblFolio.Location = new System.Drawing.Point(598, 26);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(60, 28);
            this.lblFolio.TabIndex = 26;
            this.lblFolio.Text = "Folio:";
            // 
            // lblIdPaciente
            // 
            this.lblIdPaciente.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdPaciente.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblIdPaciente.Location = new System.Drawing.Point(6, 101);
            this.lblIdPaciente.Name = "lblIdPaciente";
            this.lblIdPaciente.Size = new System.Drawing.Size(121, 28);
            this.lblIdPaciente.TabIndex = 27;
            this.lblIdPaciente.Text = "IdPaciente:";
            // 
            // lblidConsultorio
            // 
            this.lblidConsultorio.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblidConsultorio.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblidConsultorio.Location = new System.Drawing.Point(456, 102);
            this.lblidConsultorio.Name = "lblidConsultorio";
            this.lblidConsultorio.Size = new System.Drawing.Size(141, 28);
            this.lblidConsultorio.TabIndex = 28;
            this.lblidConsultorio.Text = "IdConsultorio:";
            // 
            // lblFecha
            // 
            this.lblFecha.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblFecha.Location = new System.Drawing.Point(17, 39);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(140, 28);
            this.lblFecha.TabIndex = 29;
            this.lblFecha.Text = "Fecha:";
            // 
            // lblFechaPro
            // 
            this.lblFechaPro.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaPro.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblFechaPro.Location = new System.Drawing.Point(456, 39);
            this.lblFechaPro.Name = "lblFechaPro";
            this.lblFechaPro.Size = new System.Drawing.Size(140, 28);
            this.lblFechaPro.TabIndex = 30;
            this.lblFechaPro.Text = "Fecha Prox:";
            // 
            // lblOvservaciones
            // 
            this.lblOvservaciones.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOvservaciones.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblOvservaciones.Location = new System.Drawing.Point(17, 90);
            this.lblOvservaciones.Name = "lblOvservaciones";
            this.lblOvservaciones.Size = new System.Drawing.Size(157, 28);
            this.lblOvservaciones.TabIndex = 31;
            this.lblOvservaciones.Text = "Observaciones";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNConsultorio);
            this.groupBox1.Controls.Add(this.txtPaciente);
            this.groupBox1.Controls.Add(this.lblPaciente);
            this.groupBox1.Controls.Add(this.lblConsultorio);
            this.groupBox1.Controls.Add(this.txtIdConsultorio);
            this.groupBox1.Controls.Add(this.txtIdPaciente);
            this.groupBox1.Controls.Add(this.txtMedico);
            this.groupBox1.Controls.Add(this.lblMedico);
            this.groupBox1.Controls.Add(this.txtIdMedico);
            this.groupBox1.Controls.Add(this.txtFolio);
            this.groupBox1.Controls.Add(this.lblFolio);
            this.groupBox1.Controls.Add(this.lblIdMedico);
            this.groupBox1.Controls.Add(this.lblIdPaciente);
            this.groupBox1.Controls.Add(this.lblidConsultorio);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(22, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(808, 201);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtNConsultorio
            // 
            this.txtNConsultorio.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNConsultorio.Location = new System.Drawing.Point(602, 144);
            this.txtNConsultorio.Name = "txtNConsultorio";
            this.txtNConsultorio.Size = new System.Drawing.Size(200, 26);
            this.txtNConsultorio.TabIndex = 38;
            this.txtNConsultorio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPaciente
            // 
            this.txtPaciente.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaciente.Location = new System.Drawing.Point(133, 147);
            this.txtPaciente.Name = "txtPaciente";
            this.txtPaciente.Size = new System.Drawing.Size(289, 26);
            this.txtPaciente.TabIndex = 37;
            this.txtPaciente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPaciente
            // 
            this.lblPaciente.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaciente.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblPaciente.Location = new System.Drawing.Point(6, 148);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(121, 28);
            this.lblPaciente.TabIndex = 36;
            this.lblPaciente.Text = "IdPaciente:";
            // 
            // lblConsultorio
            // 
            this.lblConsultorio.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultorio.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblConsultorio.Location = new System.Drawing.Point(479, 145);
            this.lblConsultorio.Name = "lblConsultorio";
            this.lblConsultorio.Size = new System.Drawing.Size(141, 28);
            this.lblConsultorio.TabIndex = 35;
            this.lblConsultorio.Text = "Consultorio:";
            // 
            // txtIdConsultorio
            // 
            this.txtIdConsultorio.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdConsultorio.Location = new System.Drawing.Point(602, 100);
            this.txtIdConsultorio.Name = "txtIdConsultorio";
            this.txtIdConsultorio.Size = new System.Drawing.Size(100, 26);
            this.txtIdConsultorio.TabIndex = 34;
            this.txtIdConsultorio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtIdPaciente
            // 
            this.txtIdPaciente.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdPaciente.Location = new System.Drawing.Point(133, 102);
            this.txtIdPaciente.Name = "txtIdPaciente";
            this.txtIdPaciente.Size = new System.Drawing.Size(100, 26);
            this.txtIdPaciente.TabIndex = 33;
            this.txtIdPaciente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtMedico
            // 
            this.txtMedico.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedico.Location = new System.Drawing.Point(133, 63);
            this.txtMedico.Name = "txtMedico";
            this.txtMedico.Size = new System.Drawing.Size(289, 26);
            this.txtMedico.TabIndex = 32;
            this.txtMedico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblMedico
            // 
            this.lblMedico.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMedico.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblMedico.Location = new System.Drawing.Point(6, 63);
            this.lblMedico.Name = "lblMedico";
            this.lblMedico.Size = new System.Drawing.Size(95, 28);
            this.lblMedico.TabIndex = 31;
            this.lblMedico.Text = "Medico:";
            // 
            // txtIdMedico
            // 
            this.txtIdMedico.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdMedico.Location = new System.Drawing.Point(133, 27);
            this.txtIdMedico.Name = "txtIdMedico";
            this.txtIdMedico.Size = new System.Drawing.Size(100, 26);
            this.txtIdMedico.TabIndex = 30;
            this.txtIdMedico.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtFolio
            // 
            this.txtFolio.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFolio.Location = new System.Drawing.Point(702, 24);
            this.txtFolio.Name = "txtFolio";
            this.txtFolio.Size = new System.Drawing.Size(100, 26);
            this.txtFolio.TabIndex = 29;
            this.txtFolio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // grbCita
            // 
            this.grbCita.Controls.Add(this.lblFormato);
            this.grbCita.Controls.Add(this.txtObserva);
            this.grbCita.Controls.Add(this.txtFechaProx);
            this.grbCita.Controls.Add(this.txtFecha);
            this.grbCita.Controls.Add(this.lblFechaPro);
            this.grbCita.Controls.Add(this.lblOvservaciones);
            this.grbCita.Controls.Add(this.lblFecha);
            this.grbCita.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbCita.Location = new System.Drawing.Point(22, 267);
            this.grbCita.Name = "grbCita";
            this.grbCita.Size = new System.Drawing.Size(808, 239);
            this.grbCita.TabIndex = 33;
            this.grbCita.TabStop = false;
            this.grbCita.Text = "Cita";
            // 
            // lblFormato
            // 
            this.lblFormato.AutoSize = true;
            this.lblFormato.Location = new System.Drawing.Point(565, 92);
            this.lblFormato.Name = "lblFormato";
            this.lblFormato.Size = new System.Drawing.Size(227, 20);
            this.lblFormato.TabIndex = 42;
            this.lblFormato.Text = "Formato de fecha: 31/01/2019";
            // 
            // txtObserva
            // 
            this.txtObserva.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObserva.Location = new System.Drawing.Point(21, 121);
            this.txtObserva.Multiline = true;
            this.txtObserva.Name = "txtObserva";
            this.txtObserva.Size = new System.Drawing.Size(771, 101);
            this.txtObserva.TabIndex = 41;
            // 
            // txtFechaProx
            // 
            this.txtFechaProx.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaProx.Location = new System.Drawing.Point(602, 41);
            this.txtFechaProx.Name = "txtFechaProx";
            this.txtFechaProx.Size = new System.Drawing.Size(200, 26);
            this.txtFechaProx.TabIndex = 40;
            this.txtFechaProx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtFecha
            // 
            this.txtFecha.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecha.Location = new System.Drawing.Point(114, 38);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Size = new System.Drawing.Size(200, 26);
            this.txtFecha.TabIndex = 39;
            this.txtFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnMedico
            // 
            this.btnMedico.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnMedico.FlatAppearance.BorderSize = 0;
            this.btnMedico.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMedico.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMedico.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnMedico.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMedico.Location = new System.Drawing.Point(25, 529);
            this.btnMedico.Name = "btnMedico";
            this.btnMedico.Size = new System.Drawing.Size(124, 31);
            this.btnMedico.TabIndex = 34;
            this.btnMedico.Text = "Medico";
            this.btnMedico.UseVisualStyleBackColor = false;
            this.btnMedico.Click += new System.EventHandler(this.btnMedico_Click);
            // 
            // btnPaciente
            // 
            this.btnPaciente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnPaciente.FlatAppearance.BorderSize = 0;
            this.btnPaciente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnPaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPaciente.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaciente.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnPaciente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPaciente.Location = new System.Drawing.Point(154, 529);
            this.btnPaciente.Name = "btnPaciente";
            this.btnPaciente.Size = new System.Drawing.Size(124, 31);
            this.btnPaciente.TabIndex = 35;
            this.btnPaciente.Text = "Paciente";
            this.btnPaciente.UseVisualStyleBackColor = false;
            this.btnPaciente.Click += new System.EventHandler(this.btnPaciente_Click);
            // 
            // btnConsultorio
            // 
            this.btnConsultorio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnConsultorio.FlatAppearance.BorderSize = 0;
            this.btnConsultorio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnConsultorio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultorio.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultorio.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnConsultorio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultorio.Location = new System.Drawing.Point(283, 529);
            this.btnConsultorio.Name = "btnConsultorio";
            this.btnConsultorio.Size = new System.Drawing.Size(124, 31);
            this.btnConsultorio.TabIndex = 36;
            this.btnConsultorio.Text = "Consultorio";
            this.btnConsultorio.UseVisualStyleBackColor = false;
            this.btnConsultorio.Click += new System.EventHandler(this.btnConsultorio_Click);
            // 
            // btnCitas
            // 
            this.btnCitas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnCitas.FlatAppearance.BorderSize = 0;
            this.btnCitas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnCitas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCitas.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCitas.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnCitas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCitas.Location = new System.Drawing.Point(707, 529);
            this.btnCitas.Name = "btnCitas";
            this.btnCitas.Size = new System.Drawing.Size(124, 31);
            this.btnCitas.TabIndex = 37;
            this.btnCitas.Text = "Ver Citas";
            this.btnCitas.UseVisualStyleBackColor = false;
            this.btnCitas.Click += new System.EventHandler(this.btnCitas_Click);
            // 
            // btnAgendar
            // 
            this.btnAgendar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnAgendar.FlatAppearance.BorderSize = 0;
            this.btnAgendar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.btnAgendar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgendar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgendar.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnAgendar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgendar.Location = new System.Drawing.Point(578, 529);
            this.btnAgendar.Name = "btnAgendar";
            this.btnAgendar.Size = new System.Drawing.Size(124, 31);
            this.btnAgendar.TabIndex = 38;
            this.btnAgendar.Text = "Agendar";
            this.btnAgendar.UseVisualStyleBackColor = false;
            this.btnAgendar.Click += new System.EventHandler(this.btnAgendar_Click);
            // 
            // Citas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 572);
            this.Controls.Add(this.btnAgendar);
            this.Controls.Add(this.btnCitas);
            this.Controls.Add(this.btnConsultorio);
            this.Controls.Add(this.btnPaciente);
            this.Controls.Add(this.btnMedico);
            this.Controls.Add(this.grbCita);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblCita);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Citas";
            this.Text = "Citas";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grbCita.ResumeLayout(false);
            this.grbCita.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCita;
        private System.Windows.Forms.Label lblIdMedico;
        private System.Windows.Forms.Label lblFolio;
        private System.Windows.Forms.Label lblIdPaciente;
        private System.Windows.Forms.Label lblidConsultorio;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label lblFechaPro;
        private System.Windows.Forms.Label lblOvservaciones;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNConsultorio;
        private System.Windows.Forms.TextBox txtPaciente;
        private System.Windows.Forms.Label lblPaciente;
        private System.Windows.Forms.Label lblConsultorio;
        private System.Windows.Forms.TextBox txtIdConsultorio;
        private System.Windows.Forms.TextBox txtMedico;
        private System.Windows.Forms.Label lblMedico;
        private System.Windows.Forms.TextBox txtIdMedico;
        private System.Windows.Forms.TextBox txtFolio;
        private System.Windows.Forms.GroupBox grbCita;
        private System.Windows.Forms.TextBox txtObserva;
        private System.Windows.Forms.TextBox txtFechaProx;
        private System.Windows.Forms.TextBox txtFecha;
        public System.Windows.Forms.Button btnMedico;
        public System.Windows.Forms.Button btnPaciente;
        public System.Windows.Forms.Button btnConsultorio;
        public System.Windows.Forms.Button btnCitas;
        public System.Windows.Forms.Button btnAgendar;
        private System.Windows.Forms.Label lblFormato;
        public System.Windows.Forms.TextBox txtIdPaciente;
    }
}