﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Hospital
{
    public partial class Pacientes : Form
    {
        public Pacientes()
        {
            InitializeComponent();
        }

        private  void Limpiar()
        {
            txtIdPaciente.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtSex.Clear();
            txtEdad.Clear();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            
            try
            {
                string cmd = string.Format("INSERT INTO dbo.Paciente (Nombre, Apellido, Sexo, Edad)" +
                "VALUES ('{0}','{1}','{2}','{3}')", txtNombre.Text.Trim(), txtApellido.Text.Trim(), txtSex.Text.Trim(), txtEdad.Text.Trim());
                DataSet ds = Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se registrado un nuevo paciente");
                Limpiar();
            }
            catch (Exception error)
            {
                MessageBox.Show("Error al registrar el paciente Verifique esten llenos los campos. "+error.Message);
            }

            
        }
       

        private void Pacientes_Load(object sender, EventArgs e)
        {
           
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultarPacientes cp = new ConsultarPacientes();
            cp.ShowDialog();

            if (cp.DialogResult == DialogResult.OK)
            {
                txtIdPaciente.Text = cp.dataGridView1.Rows[cp.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtNombre.Text = cp.dataGridView1.Rows[cp.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
                txtApellido.Text = cp.dataGridView1.Rows[cp.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
                txtSex.Text = cp.dataGridView1.Rows[cp.dataGridView1.CurrentRow.Index].Cells[3].Value.ToString();
                txtEdad.Text = cp.dataGridView1.Rows[cp.dataGridView1.CurrentRow.Index].Cells[4].Value.ToString();
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                string cmd = "DELETE FROM dbo.Paciente WHERE IdPaciente = " + txtIdPaciente.Text.Trim();
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado un paciente. " ,"Eliminar");
                Limpiar();


            }
            catch (Exception error)
            {
                MessageBox.Show("Error al tratar de eliminar el paciente. " + error.Message);
            }


        }
    }
}
