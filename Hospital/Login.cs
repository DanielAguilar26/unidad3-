﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using MiLibreria;
using System.Data;
using System.Data.SqlClient;

namespace Hospital
{
    public partial class Login : Form
    {
        public Login()
        {
            Thread time = new Thread(new ThreadStart(SplasStart));
            time.Start();
            Thread.Sleep(5000);
            InitializeComponent();       
            time.Abort();
        }
        
        public void SplasStart()
        {
            Application.Run(new Splash());
            
        }

        public static String id = "";

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "USUARIO")
                txtUsuario.Text = "";
                
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "")
                txtUsuario.Text = "USUARIO";
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            if (txtPass.Text == "PASSWORD")
            { 
                txtPass.Text = "";
                txtPass.UseSystemPasswordChar = true;
            }
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            if (txtPass.Text == "")
            { 
                txtPass.UseSystemPasswordChar = false;
                txtPass.Text = "PASSWORD";
            }


        }

        private void btnAcceder_Click(object sender, EventArgs e)
        {
            try
            {
                string cmd = string.Format("SELECT * FROM dbo.Medico WHERE Account='{0}' AND Mpassword='{1}'", txtUsuario.Text.Trim(), txtPass.Text.Trim());           
                DataSet ds = Utilidades.Ejecutar(cmd);

                id = ds.Tables[0].Rows[0]["IdMedico"].ToString().Trim();

                string account = ds.Tables[0].Rows[0]["Account"].ToString().Trim();
                string mpass = ds.Tables[0].Rows[0]["Mpassword"].ToString().Trim();

                if (account == txtUsuario.Text.Trim() && mpass == txtPass.Text.Trim())
                {
                    MessageBox.Show("Se ha iniciado secion.");
                    Principal p= new Principal();
                    this.Hide();
                    p.Show();
                    
                }
                
                
            }
            catch (Exception error)
            {
                
                MessageBox.Show("usuario o password incorrecto."+error.Message);
            }
        }

        private void lplRecuperar_Click(object sender, EventArgs e)
        {
            Medicos pm = new Medicos();
            pm.ShowDialog();
        }
    }
}
