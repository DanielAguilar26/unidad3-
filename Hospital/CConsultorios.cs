﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace Hospital
{
    public partial class CConsultorios : Consultas
    {
        public CConsultorios()
        {
            InitializeComponent();
        }

        private void CConsultorios_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LlenarDataGVM("Consultorio").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNombreM.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "SELECT * FROM Consultorio Where NombreConsultorio LIKE ('%" + txtNombreM.Text.Trim() + "%')";
                    ds = Utilidades.Ejecutar(cmd);

                    dataGridView1.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error. " + error.Message);
                }
            }
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnResgistar_Click(object sender, EventArgs e)
        {
            try
            {
                string cmd = string.Format("INSERT INTO dbo.Consultorio (NombreConsultorio) VALUES ('{0}')", txtNombreM.Text.Trim());
                DataSet ds = Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se registrado un nuevo consultorio");
            }
            catch(Exception error)
            {

                MessageBox.Show("Ha ocurrido un error. " + error.Message);
            }
            
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                string cmd = "DELETE FROM dbo.Consultorio WHERE IdConsultorio = " + txtIdCon.Text.Trim();
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha eliminado un paciente. ", "Eliminar");
                txtIdCon.Clear();
                txtNombreM.Clear();
              


            }
            catch (Exception error)
            {
                MessageBox.Show("Error al tratar de eliminar el consultorio. " + error.Message);
            }
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            CConsultorios cc = new CConsultorios();
            cc.ShowDialog();

            if (cc.DialogResult == DialogResult.OK)
            {
               txtIdCon.Text = cc.dataGridView1.Rows[cc.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtNombreM.Text = cc.dataGridView1.Rows[cc.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
            }
        }
    }
}
